** This was a Fork From https://github.com/bennojoy/openldap_server.git **

Openldap Server
===============

This roles installs the OpenLDAP server on the target machine.

Dependencies
------------

None

Variables
---------

[Role Variables](defaults/main.yml)

Examples Playbook
-----------------

[Test Playbook without SSL](molecule/default/playbook.yml)

[Test Playbook with SSL](molecule/default/playbook_ssl.yml)


Dependencies
------------

None

License
-------

[License](LICENSE)
